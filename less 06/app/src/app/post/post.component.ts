import {Component, OnDestroy, OnInit} from '@angular/core'
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {PostsService} from "../posts.service";
import {Post} from "../models/post.model";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit, OnDestroy{
  post: Post = new Post();
  subscription: Subscription = new Subscription();
  constructor(
    private route: ActivatedRoute,
    private postsService: PostsService,
    private router: Router,
    ){}
  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.post = data.post
    })
    // this.post = this.postsService.getById(+this.route.snapshot.params.id)
    // this.subscription.add(
    //   this.route.params.subscribe(params => {
    //     this.post = this.postsService.getById(+params.id)
    //   })
    // )
  }
  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  getPost() {
    this.router.navigate(['/posts', 44], {
      queryParams: {id: '44'}
    })
  }
}
