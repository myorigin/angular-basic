import {Component, OnInit} from '@angular/core'
import {PostsService} from '../posts.service'
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit{
  showIds = false;
  constructor(
    public postsService: PostsService,
    private route: ActivatedRoute,
    private router: Router,
  ) {}
  ngOnInit(): void {
    console.log(this.route)
    this.route.queryParams.subscribe(query => {
      this.showIds = !!query.showIds;
    })
  }

  onShowIds() {
    this.router.navigate(['/posts'], {
      queryParams: {showIds: true},
      fragment: 'another'
    })
  }
}
