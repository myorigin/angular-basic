import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAuth = false;
  login() {
    this.isAuth = true
  }
  logout() {
    this.isAuth = false
  }
  isAthenticated(): Promise<boolean>{
    return new Promise<boolean>(resolve => {
      setTimeout(() => {resolve(this.isAuth)}, 1000)
    })
  }
}
