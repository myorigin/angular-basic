
export class Post {
  title: string;
  text: string;
  id?: number;

  constructor() {
    this.text = '';
    this.title = ''
  }
}
