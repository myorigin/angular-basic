import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exp-directives',
  templateUrl: './exp-directives.component.html',
  styleUrls: ['./exp-directives.component.css']
})
export class ExpDirectivesComponent implements OnInit {
  isVisible = true
  constructor() { }

  ngOnInit(): void {
  }

}
