import { Pipe, PipeTransform } from '@angular/core';
import {Post} from "../exp-pipes/exp-pipes.component";
import {filter} from "rxjs/operators";

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {

  transform(posts: Post[], search: string, field: keyof Post): Post[] {
    if (!search) {
      return posts;
    }
    console.log(field)
    return posts.filter(post => post[field].toLowerCase().includes(search.toLowerCase()))
  }
}
