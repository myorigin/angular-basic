import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {StyleDirective} from "./directives/style.directive";
import { ExpDirectivesComponent } from './exp-directives/exp-directives.component';
import { NotIfDirective } from './directives/not-if.directive';
import { ExpPipesComponent } from './exp-pipes/exp-pipes.component';
import {MultPipe} from "./pipes/mult.pipe";
import {FormsModule} from "@angular/forms";
import { FilterPipe } from './pipes/filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    StyleDirective,
    ExpDirectivesComponent,
    NotIfDirective,
    ExpPipesComponent,
    MultPipe,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
