import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appNotIf]'
})
export class NotIfDirective {
  @Input('appNotIf') set ifNot(isVisible: boolean){
    if (!isVisible) {
      this.container.createEmbeddedView(this.template)
    } else {
      this.container.clear()
    }
  }
  constructor(private template: TemplateRef<any>, private container: ViewContainerRef) {
    console.log(this)

  }

}
