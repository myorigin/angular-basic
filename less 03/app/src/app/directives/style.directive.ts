import {Directive, ElementRef, HostBinding, HostListener, Input, Renderer2} from "@angular/core";

@Directive({
  selector: '[appStyle]'
})
export class StyleDirective {
  @Input('appStyle') color: string
  @Input() fontSize: string
  @Input() dStyle: { border: string }
  constructor(private el: ElementRef, private r: Renderer2) {
    this.el.nativeElement.style.color = 'red'
    this.r.setStyle(this.el.nativeElement, 'color', 'blue')
    // console.log(this.el, this.color)
  }
  @HostBinding('style.color') elColor: string | null
  @HostListener('click', ['$event']) onClick(el: Element) {
    console.log(el)
  }
  @HostListener('mouseenter')onEnter() {
    this.elColor = this.color
    // this.r.setStyle(this.el.nativeElement, 'color', this.color)
    // this.r.setStyle(this.el.nativeElement, 'fontSize', this.fontSize)
    // this.r.setStyle(this.el.nativeElement, 'border', this.dStyle.border)
  }
  @HostListener('mouseleave')onLeave() {
    this.elColor = null

    // this.r.setStyle(this.el.nativeElement, 'color', null)
    // this.r.setStyle(this.el.nativeElement, 'fontSize', null)
    // this.r.setStyle(this.el.nativeElement, 'border', null)
  }
}
