import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
export interface Post {
  name: string,
  text: string,
}
@Component({
  selector: 'app-exp-pipes',
  templateUrl: './exp-pipes.component.html',
  styleUrls: ['./exp-pipes.component.css']
})
export class ExpPipesComponent implements OnInit {
  num = Math.random()
  str = 'Some text'
  // date = new Date()
  float = 0.05257
  obj = {
    d: {
      dd:'dd',
      c: 15
    }
  }
  field: keyof Post = 'text'
  search: string
  posts: Post[] = [
    {name: 'the language', text: 'text description'},
    {name: 'the country', text: 'country description'},
    {name: 'the animals', text: 'there are a lot of cats'},
  ]
  p: Promise<string> = new Promise<string>((resolve => {
    setTimeout(() => {
      resolve('hello from promise')
    }, 3000)
  }))

  date: Observable<Date> = new Observable<Date>((obj) => {
    setInterval(() => {
      obj.next(new Date())
    }, 1000)
  })
  constructor() { }

  ngOnInit(): void {
  }
  push() {
    this.posts.unshift(
      {name: 'Angular', text: 'angular description'},
    )
  }
}
