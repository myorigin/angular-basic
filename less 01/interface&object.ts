//  interface
// optional prop ?
// extends

interface IUser {
    name: string,
    age: number,
    // logInfo: () => void,
    // logInfo?: () => void,
    logInfo(): void,
    id?: any
}
interface IAdmin extends IUser{
    role: string
}
// const user: IUser  = {
//     name: 'Liubov',
//     age: 25,
//     logInfo() {
//         console.log(this.name + ' ' + this.age)
//     }
// }

const user: IUser  = {
    name: 'Liubov',
    age: 25,
    logInfo() {
        console.log(this.name + ' ' + this.age)
    },
    id: 3
}
