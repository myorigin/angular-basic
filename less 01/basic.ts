// https://www.typescriptlang.org/docs/handbook/typescript-tooling-in-5-minutes.html
// install
// npm i -g typescript
// tsc [file] -- basic compilation
//
let str: string = 'Hello Typescript'
let num: number = 42
let isActive: boolean = false

let strArray: string[] = ['H', 'e', 'l']
let numArray: Array<number> = [1, 1, 2, 3]

enum Direction {
    Up,
    Down,
    Left,
    Right,
}
