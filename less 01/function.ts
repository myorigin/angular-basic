function logInfo(name: string, age: string): void {
    console.log(`info: ${name}, ${age}`)
}

function calc(a: number, b: number): number {
    return  a + b;
}

// отримання помилки
calc(5, '6');

function calc1(a: number, b: number | string): number {
    if (typeof b === 'string') {
        b = +b
    }
    return  a + b;
}
calc1(5, '6');
// type union |

