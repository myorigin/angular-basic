const numbers: number[] = [1,2,3]

// тип масив і його generic type number
const arr: Array<number> = [1,2,3]

interface IUser {
    id: number,
    name: string,
    age: number,
}
const users: Array<IUser> = [
    {id: 1, name: 'N', age: 12},
    {id: 1, name: 'N', age: 11},
    {id: 1, name: 'N', age: 12},
]

const users2: IUser[] = [
    {id: 1, name: 'N', age: 12},
    {id: 1, name: 'N', age: 11},
]
