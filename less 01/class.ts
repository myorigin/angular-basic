// class Server {
//     static VERSION = '1.0.0'
//     name: string
//     ip: string
//     status: string = 'on'
//     constructor(name, ip) {
//         this.name = name
//         this.ip = ip
//     }
//
//     turnOn() {
//         this.status = 'on'
//     }
//     turnOff() {
//         this.status = 'off'
//     }
//
//     getStatus(): string {
//         return this.status
//     }
// }
//

class Server {
    static VERSION = '1.0.0'
    private status: string = 'on'
    constructor(public name: string, protected ip: number) {
    }

    turnOn() {
        this.status = 'on'
    }
    turnOff() {
        this.status = 'off'
    }

    getStatus(): string {
        return this.status
    }
}

const server = new Server('S3', 1234);
