import { Component } from '@angular/core';
export interface Post {
  name: string,
  text: string,
  id?: number
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  posts: Post[] = [
    {name: "name asdas ahjskd  ahsd", text: 'some descr', id: 1},
    {name: "name1", text: 'some descr', id: 2},
    {name: "name2", text: 'some descr', id: 3},
    {name: "name3", text: 'some descr', id: 4},
  ]
  constructor() {
  }
  createPost(post: Post) {
    this.posts.push(post)
  }
  removePost(postId: number) {
    this.posts = this.posts.filter(({id}) => id !== postId)
  }
}
