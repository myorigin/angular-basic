import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Post} from "../app.component";

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  name = ''
  text = ''
  @Output() onAdd: EventEmitter<Post> = new EventEmitter<Post>()
  @ViewChild('inputRef') inputRef: ElementRef
  constructor() { }

  ngOnInit(): void {
  }
  createPost() {
    if(this.name.trim() && this.text.trim()) {
      const post = {
        name: this.name,
        text: this.text
      }
      this.text = this.name = ''
      this.onAdd.emit(post)
    }
  }
  focus() {
    console.log(this.inputRef)
    this.inputRef.nativeElement.focus()
  }
}
