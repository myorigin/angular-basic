import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  DoCheck,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation
} from '@angular/core';
import {Post} from "../app.component";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostComponent implements OnInit, OnChanges, DoCheck, OnDestroy {
  @Input() post: Post
  @ContentChild('tag', {static: true}) tagRef: ElementRef
  @Output() onRemove: EventEmitter<number> = new EventEmitter<number>()
  constructor() {
    console.log('constructor')
  }
  ngOnChanges(changes:SimpleChanges) {
    console.log('ngOnChanges', changes)
  }
  ngOnInit(): void {
    console.log('ngOnInit')
  }
  ngDoCheck() {
    console.log('ngDoCheck')
  }
  ngOnDestroy() {
    console.log('ngOnDestroy')
  }
  remove() {
    this.onRemove.emit(this.post.id)
  }


}
