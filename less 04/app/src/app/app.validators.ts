import {AbstractControl, ValidationErrors} from "@angular/forms";
import {resolve} from "@angular/compiler-cli/src/ngtsc/file_system";

interface Error {
  [key: string]: boolean
}

export class AppValidators {
  static restrictedEmail(control: AbstractControl): ValidationErrors | null {
    if (['test@gmail.com'].includes(control.value)) {
      return {restrictedEmail: true}
    }
    return null
  }

  static asyncEmail(control: AbstractControl): Promise<ValidationErrors | null> {
    return new Promise(resolve => {
      if (control.value === 'async@test.com') {
        setTimeout(() => {
          resolve({asyncEmail: true})
        }, 2000)
      } else {
        resolve( null )
      }
    })
  }
}
