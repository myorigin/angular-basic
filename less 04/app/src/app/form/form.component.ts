import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AppValidators} from "../app.validators";
interface Country {
  city: string,
  code: string,
  country: string
}
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  login: FormGroup
  register: FormGroup
  countries: Country[] = [
    {country: 'Ukraine', city: 'Kiev', code: 'ua'},
    {country: 'Poland', city: 'Varshava', code: 'pl'},
  ]
  constructor(private fb:FormBuilder) { }

  ngOnInit(): void {
    this.initLoginForm()
    this.initRegisterForm();
  }
  initLoginForm() {
    this.login = new FormGroup({
      email: new FormControl(
        '',
        [Validators.email, Validators.required, AppValidators.restrictedEmail],
        AppValidators.asyncEmail,
      ),
      password: new FormControl('', [Validators.required, Validators.minLength(4)])
    })
  }
  initRegisterForm() {
    this.register = this.fb.group({
      name: ['', Validators.required],
      address: this.fb.group({
        country: ['ua'],
        city: ['', Validators.required]
      }),
      skills: this.fb.array([
        ['', Validators.required],
      ]),
    });
  }

  onLogin() {
    if(this.login.valid) {
      // console.log(this.login.value)
      this.login.reset()
    }
  }

  onRegister() {
    console.log(this.register.value)
  }

  onCountryChange() {

    const code = this.register.get('address')?.get('country')?.value;
    this.register.patchValue({
      address: {
        city: this.countries.find((country) => country.code === code)?.city
      }
    })
  }

  addSkill() {
    const controller = this.fb.control('', Validators.required);
    (this.register.get('skills') as FormArray).push(controller)
    // (<FormArray>this.register.get('skills')).push(controller);
  }
  getSkillsController() {
    return (this.register.get('skills') as FormArray)
  }
}
