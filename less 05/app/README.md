#### RxJs lib для роботи із даними та асинхронністю
бібліотека для компонування асннхронних програм та програм на основі подій 
будь-який код можна зробити стрімом, на який можна підписатись і ловити івенти про зміну
pattern Observable

import із rxjs and rxjs/operators

#### Створення
за допомогою конструктору 
new Observable((observer) => {
  next: value => {},
  error: err => {},
  complete: () => {}
})


observer.error('error msg')}, 2000 - обробка через try catch

на відміну від проміс - можна викликати будь-яку кількість раз

#### unsubscribe
1 item
push as array and forEach,

// array 

subscription: Subscription = new Subscription();
subscription.add(req.subscribe())
subscription.unsubscribe()

#### operators
pipe-- для передачі кількох операторів в середину
filter
map
switchMap - зміна напрямку потоку
  intervalStream$
  .pipe(
    filter(t => t%2 === 0),
    map(t => `mapped time ${t}`)
  )
  .subscribe((time) => {
    console.log(time)
  })

#### переваги
 кілька раз викликати можна

#### Subject
Subject - дає можливість емітити  власні івент та підписуватись на них
-- сторити 
stream$: Subject<number> = new Subject<number>();
-- підписатись
-- емітити на якісь подію

Services
Сервіси - класи для роботи із даними, едина сутність, яка може бути оголошена без декоратору

#### Create service
Створення вручну
  /services
  
import into module/providers

Create global and local counter component

#### Global service
with @Injectable({
  providedIn: 'root'|| LocalModule
}) --можна інжектити в середину інші сервіси
remove from providers
#### terminal
  ng g s name --skip-tests
#### local service
in component import providers: [localCounterService]
#### show diff (scope)
create a few components

#### inject service into another

#### HttpClient
import service
import module
import {HttpClientModule} from "@angular/common/http";

get data from json placeholder
подив в консоль, що немає XHR запиту, тому що все працює на RxJS

https://jsonplaceholder.typicode.com/users/1/todos?_limit=2

json placeholder

Render list
### post - create
creat form, create and send todo, show loader
this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?_limit=2')
  .pipe(delay(1000))
  .subscribe(value => {
  this.loading = false
  this.todos = value;
})

### remove todo
delete

show message about no todos

#### move into service
Всю логіку по роботі із даними переноисити в сервіси
Інтерфейси також в сервіси

#### status completed and put
complete(todoId: number): Observable<Todo>{
  return this.http.put<Todo>('https://jsonplaceholder.typicode.com/todos/' + todoId, {
  competed: true
  })
}

get error on update new todos
#### errors handler
2-а способи
for HttpClient
  error => this.err = err.msg
  pipe(
    finally
  )

for RxJS
#### headers & params
    const headers = new HttpHeaders({'header': Math.random().toString()})

  new HttpHeaders()
  new HttpParams().set()
                  .append('name', 'value')

#### interceptor
це клас,який імплементить інтерфейс HttpInterceptor
create custom provider

export class AuthInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('interceptor------', req)
    return next.handle(req);
  }
}

const INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  useClass: AuthInterceptor,
  multi: true,
}

встановлення нового хедера наприклад
