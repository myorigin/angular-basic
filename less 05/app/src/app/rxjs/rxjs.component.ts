import { Component, OnInit } from '@angular/core';
import {interval, Observable, Subject, Subscription} from "rxjs";
import {filter, map} from "rxjs/operators";

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.scss']
})
export class RxjsComponent implements OnInit {
  subscription: Subscription = new Subscription();
  stream$: Subject<number> = new Subject<number>();
  counter = 0;
  constructor() { }

  ngOnInit(): void {
    const intervalSub$ = interval(1000);
    const stream$ = new Observable((observable) => {
      observable.next(1);
      setTimeout( () => {
        observable.next(2)
      }, 1000)

      setTimeout( () => {
        observable.error('Error message')
      }, 3000)
      setTimeout( () => {
        observable.complete()
      }, 4000)
    })

    this.subscription.add(
      stream$.subscribe({
        next: (val) => {
          console.log('next--', val)
        },
        error: err => console.error(err),
        complete: () => console.log('complete')
      })
      )
    this.subscription.add(
      intervalSub$
        .pipe(
          filter(t => t%2 === 0),
          map(t => `mapped time ${t}`),
        )
        .subscribe(val => console.log(val))
    )

    this.subscription.add(
      this.stream$.subscribe(val => console.log('counter--', val))
    )
  }

  unsc() {
    console.log(this.subscription);
    this.subscription.unsubscribe()
  }

  increase() {
    this.counter++;
    this.stream$.next(this.counter)
  }
}
