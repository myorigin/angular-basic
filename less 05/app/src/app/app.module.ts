import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpComponent } from './http/http.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { CounterComponent } from './counter/counter.component';
import { LocalCounterComponent } from './local-counter/local-counter.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AuthInterceptor} from "./services/auth.interceptor";

const INTERCEPTOR_PROVIDER = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: AuthInterceptor,
}
@NgModule({
  declarations: [
    AppComponent,
    HttpComponent,
    RxjsComponent,
    CounterComponent,
    LocalCounterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    INTERCEPTOR_PROVIDER
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
