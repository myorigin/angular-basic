import { Component, OnInit } from '@angular/core';
import {Todos, TodosService} from "../services/todos.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {finalize} from "rxjs/operators";

@Component({
  selector: 'app-http',
  templateUrl: './http.component.html',
  styleUrls: ['./http.component.scss']
})
export class HttpComponent implements OnInit {
  loading = false;
  list: Todos[] = [];
  form: FormGroup;
  error = ''
  constructor(private todosService: TodosService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.get();
    this.initForm()
  }
  initForm() {
    this.form = this.fb.group({
      title: ['', Validators.required],
      completed: false,
    })
  }
  onRemove(id: number) {
    this.loading = true;
    this.todosService.remove(id)
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => {
        this.list = this.list.filter(t => t.id !== id)
      })
  }

  onResume(id: number) {
    this.todosService.complete(id)
      .subscribe(val => {
       // @ts-ignore
        let todo: Todos = this.list.find(t => t.id === id);
       if (todo) {
        todo.completed = true
       }
      }, err => {
        this.error = err.message
      })

  }

  get() {
    this.loading = true;
    this.todosService.get()
      .pipe(finalize(() => this.loading = false))
      .subscribe(val => {
        this.list = val
      })
  }

  onCreate() {
    if(this.form.invalid) return;
    this.todosService.create(this.form.value)
      .subscribe(res => {
        this.list.unshift(res)
        console.log(res)
      })
  }
}
