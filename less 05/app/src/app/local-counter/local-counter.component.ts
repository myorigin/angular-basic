import { Component, OnInit } from '@angular/core';
import {LocalCounterService} from "../services/local-counter.service";

@Component({
  selector: 'app-local-counter',
  templateUrl: './local-counter.component.html',
  styleUrls: ['./local-counter.component.scss'],
  providers: [LocalCounterService]
})
export class LocalCounterComponent implements OnInit {

  constructor(public counterService: LocalCounterService) { }

  ngOnInit(): void {
  }

}
