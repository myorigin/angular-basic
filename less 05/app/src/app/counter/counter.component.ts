import { Component, OnInit } from '@angular/core';
import {AppService} from "../services/app.service";

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {

  constructor(public counterService: AppService) { }

  ngOnInit(): void {
  }

}
