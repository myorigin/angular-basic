import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class AppService{
  counter = 0;
  inc() {
    this.counter++
  }
  dec() {
    this.counter--
  }
}
