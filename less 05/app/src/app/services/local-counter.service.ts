import { Injectable } from '@angular/core';

@Injectable(
)
export class LocalCounterService {
  counter = 0;
  inc() {
    this.counter++
  }
  dec() {
    this.counter--
  }
}
