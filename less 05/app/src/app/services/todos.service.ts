import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, delay} from "rxjs/operators";


export interface Todos {
  title: string,
  completed: boolean,
  id: number,
}
@Injectable({
  providedIn: 'root'
})
export class TodosService {
  url = 'https://jsonplaceholder.typicode.com/todos';
  constructor(private http: HttpClient) { }

  get(): Observable<Todos[]> {
    let params = new HttpParams();
     params.append('_limit', 4)
    return this.http.get<Todos[]>(this.url + '?_limit=2', {
      params
    })
      .pipe(
        delay(1000),
        catchError(err => {
          console.error('Error:', err)
          return throwError(err)
        })
        )

  }
  create(todo: Todos): Observable<Todos> {
    const headers = new HttpHeaders().set('Auth', 'Custom token-------')
    return this.http.post<Todos>(this.url, todo, {
      headers
    })
  }
  remove(todoId: number): Observable<void> {
    return this.http.delete<void>(this.url + '/' + todoId)
  }
  complete(todoId: number): Observable<Todos> {
    return this.http.put<Todos>(`${this.url}/${todoId}`, {
      completed: true
    })
  }
}
