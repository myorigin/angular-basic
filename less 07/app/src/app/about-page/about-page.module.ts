import {NgModule} from "@angular/core";
import {AboutPageComponent} from "./about-page.component";
import {AboutExtraPageComponent} from "./about-extra-page/about-extra-page.component";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
const routes = [
  {
    path: '',
    component: AboutPageComponent,
    children: [
      {path: 'extra', component: AboutExtraPageComponent}
    ]
  }
]
@NgModule({
  declarations: [
    AboutPageComponent,
    AboutExtraPageComponent,
  ],
  exports: [
    AboutPageComponent,
    AboutExtraPageComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
  ]
})
export class AboutPageModule{}
